﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Painter : MonoBehaviour
{    
    //Texture actuellement projetee sur l'ecran et modifiee par les outils
    public Texture2D editedTexture;
    //Array containing every modifiable texture of the character/object
    public Sprite[] bodySprites;

    //RectTransform de la texture editee
    public RectTransform editedTextureRectTransform;    
    //RectTransform de l'objet sur lequel on "dessine" la texture
    public RectTransform drawZoneRectTrans;

    //Rect du RectTransform de l'objet sur lequel on projete;
    Rect drawRect;
    //Rect de la texture projetee
    Rect textureProjectionRect;
    //Offset de la texture projetee, qui change si on scroll ou si on zoom dans l'image
    Rect textureOffsetRect; 

    //TOOLS THINGS
    public string activeTool = "Brush";
    public Text selectedToolText;

    public int brushSize = 5;
    public int brushSizeMin = 5;
    public int brushSizeMax = 100;
    public Text brushSizeText;
    public Scrollbar brushSizeBar;

    public Color brushColor;
    public Color eraseColor;
    public Image actualColorUI;

    //MOUSE THINGS
    //Est-ce que la souris est sur une zone de dessin
    bool mouseIsOnDrawingZone;
    //Position de la souris sur l'ecran
    Vector2 mousePosOnScreen;
    //Position de la souris sur la zone de dessin
    Vector2 mousePosOnDrawingZone;
    //Position de la souris sur la texture elle-meme
    Vector2 mousePosOnTexture;
    //Position de la souris sur la texture, convertie en Vector2Int, pour avoir la position en pixel
    Vector2Int mousePosOnTextureInt;

    //Vitesse de deplacement de la texture avec la souris (Bouton Central)
    public float textureProjectionMoveSpeed = 1000f;
    //Vitesse de zoom sur la texture avec la souris (molette)
    public float textureProjectionZoomSpeed = 0.001f;
    //Niveau de zoom actuel sur la texture (Peut remplacer Texture Offset Rect W et H)
    public float textureProjectionActualZoom = 1f;

    //Ratio de la taille de la texture modifie / la taille de la zone de dessin
    Vector2 ratioTextureDZR;
    Vector2 ratioTextureDZRZoomed;

    //PAINTBUCKET THINGS
    HashSet<Vector2Int> pixelsToCheck = new HashSet<Vector2Int>();
    HashSet<Vector2Int> pixelsToColor = new HashSet<Vector2Int>();
    Color[] colorArray = new Color[0];

    void Start()
    {
        //Set the first texture to edit
        SetATextureToDraw(bodySprites[0]);

        ActualizeDrawingZonePosition();

        //Initialiser le rectangle de la texture projetee
        textureOffsetRect = new Rect(0f, 0f, 1f, 1f);

        //Calculer le ratio entre la texture et la zone de dessin DZR
        ratioTextureDZR.x = editedTextureRectTransform.rect.width / drawRect.width;
        ratioTextureDZR.y = editedTextureRectTransform.rect.height / drawRect.height;
        
        //Set tool
        ChangeTool("Brush");
        ChangeColor("black");

        //Set UI
        brushSizeText.text = brushSize.ToString();
    }

    //Mouse Input
    void Update()
    {
        mousePosOnScreen = Input.mousePosition;
        mouseIsOnDrawingZone = CheckIfMouseIsOnDrawingZone();   
        
        if (mouseIsOnDrawingZone)
        {
            //Left Click
            if (Input.GetMouseButton(0))
            {
                //Use selected tool at this position
                if (activeTool != "PaintBucket")
                {
                    UseActiveToolAtPosition(mousePosOnTextureInt.x, mousePosOnTextureInt.y);
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                if(activeTool == "PaintBucket")
                {
                    UseActiveToolAtPosition(mousePosOnTextureInt.x, mousePosOnTextureInt.y);
                }
            }

            //Middle Click - Bouger la texture
            if (Input.GetMouseButton(2))
            {
                MoveTextureProjection();
            }
            if(Input.mouseScrollDelta.y != 0)
            {
                ZoomTextureProjection(Input.mouseScrollDelta.y);
            }
        }
    }


    #region Mouse Position

    private bool CheckIfMouseIsOnDrawingZone()
    {
        if (drawRect.Contains(mousePosOnScreen, true))
        {            
            CalculateMousePositionOnDrawingZone();
            CalculateMousePositionOnEditedTexture();
            return true;
        }
        else
        {
            return false;
        }
    }

    private void CalculateMousePositionOnDrawingZone()
    {
        mousePosOnDrawingZone = mousePosOnScreen - drawRect.position;
    }

    private void CalculateMousePositionOnEditedTexture()
    {        
        Vector2 editedTextureOffset = (new Vector2(editedTextureRectTransform.sizeDelta.x * textureOffsetRect.x, editedTextureRectTransform.sizeDelta.y * textureOffsetRect.y));
        mousePosOnTexture.x = mousePosOnDrawingZone.x * ratioTextureDZR.x * textureProjectionActualZoom + editedTextureOffset.x;
        mousePosOnTexture.y = mousePosOnDrawingZone.y * ratioTextureDZR.y * textureProjectionActualZoom + editedTextureOffset.y;

        mousePosOnTextureInt = Vector2Int.RoundToInt(mousePosOnTexture);
    }

    #endregion

    #region Move and Zoom
    private void MoveTextureProjection()
    {
        //Le mouvement maximal depend du niveau de zoom, pour ne pas afficher de zone en dehors de la texture
        //Voir plus tard, si ca fonctionne avec des images rectangulaires
        float tempOffestMax = 1 - textureProjectionActualZoom;        

        textureOffsetRect.x += Input.GetAxis("Mouse X") * - textureProjectionMoveSpeed * ratioTextureDZR.x * textureProjectionActualZoom;
        textureOffsetRect.y += Input.GetAxis("Mouse Y") * - textureProjectionMoveSpeed * ratioTextureDZR.y * textureProjectionActualZoom;     
        
        //Bloquer le deplacement dans l'image
        if (textureOffsetRect.x < 0)
        {
            textureOffsetRect.x = 0;
        }
        else if (textureOffsetRect.x > tempOffestMax)
        {
            textureOffsetRect.x = tempOffestMax;
        }

        if (textureOffsetRect.y < 0)
        {
            textureOffsetRect.y = 0;
        }
        else if (textureOffsetRect.y > tempOffestMax)
        {
            textureOffsetRect.y = tempOffestMax;
        }
    }

    private void ZoomTextureProjection(float zoomDir)
    {
        textureProjectionActualZoom += textureProjectionZoomSpeed * -zoomDir;

        //Bloquer le zoom        
        //Bloquer le dezoom
        if (textureProjectionActualZoom > 1)
        {
            textureProjectionActualZoom = 1f;
        }
        else if(textureProjectionActualZoom < 0.01f) 
        {
            textureProjectionActualZoom = 0.01f;
        }
        
        textureOffsetRect.height = textureProjectionActualZoom;
        textureOffsetRect.width = textureProjectionActualZoom;

        //Actualiser le Offset de texture apres le zoom/unzoom, pour ne pas depasser de la zone
        MoveTextureProjection();
    }
    #endregion

    #region Use Selected Tool
    
    private void UseActiveToolAtPosition(int x, int y)
    {
        Vector2Int pos = new Vector2Int(x,y);

        switch (activeTool)
        {
            case "Brush":
                UseBrushAtThisPosition(pos);
                break;
            case "Eraser":
                EraseAtThisPosition(pos);
                break;
            case "PaintBucket":
                CheckIfPositionIsPaintBucketable(pos);
                break;
        }
    }

    void UseBrushAtThisPosition(Vector2Int pos)
    {
        //Calcul de la bonne position en fonction de la taille du brush
        Vector2Int correctedPosition = new Vector2Int(pos.x - brushSize / 2, pos.y - brushSize / 2);

        for (int i = 0; i < brushSize; i++)
        {
            for (int j = 0; j < brushSize; j++)
            {
                if (correctedPosition.x + i >= 0 
                    && correctedPosition.x + i < editedTextureRectTransform.sizeDelta.x
                    && correctedPosition.y + j >= 0 
                    && correctedPosition.y +j < editedTextureRectTransform.sizeDelta.y)
                {
                    editedTexture.SetPixel(correctedPosition.x + i, correctedPosition.y + j, brushColor);
                }
            }
        }

        editedTexture.Apply();
    }

    void EraseAtThisPosition(Vector2Int pos)
    {
        Vector2Int correctedPosition = new Vector2Int(pos.x - brushSize / 2, pos.y - brushSize / 2);


        for (int i = 0; i < brushSize; i++)
        {
            for (int j = 0; j < brushSize; j++)
            {
                editedTexture.SetPixel(correctedPosition.x + i, correctedPosition.y + j, eraseColor);
            }
        }
        editedTexture.Apply();
    }
    #endregion

    #region Paintbucket

    void CheckIfPositionIsPaintBucketable(Vector2Int pos)
    {        
        Color pixelColorToReplace = GetPixelColorAtPosition(pos);

        if(!IsSamePixelColor(pixelColorToReplace, brushColor))
        {
            UsePaintBucketAtPosition(pos, pixelColorToReplace);
        }
    }

    void UsePaintBucketAtPosition(Vector2Int pos, Color pixColorToReplace)
    {
        pixelsToColor = new HashSet<Vector2Int>();
        pixelsToColor.Add(pos);

        colorArray = editedTexture.GetPixels(0);

        do
        {     
            foreach (Vector2Int pixel in pixelsToColor)
            {
                AddPixelToColorArray(pixel, brushColor);
                CheckPixelsInEveryDirection(pixel, pixColorToReplace);
            }
   
            editedTexture.SetPixels(colorArray, 0);

            pixelsToColor = pixelsToCheck;
            pixelsToCheck = new HashSet<Vector2Int>();
        }
        while (pixelsToColor.Count > 0);

        editedTexture.SetPixels(colorArray, 0);
        editedTexture.Apply();
    }

    void CheckPixelsInEveryDirection(Vector2Int pos, Color color)
    {               
        List<Vector2Int> positionsToCheck = new List<Vector2Int>();

        if (pos.y + 1 < editedTexture.height)
            positionsToCheck.Add(pos + Vector2Int.up);      
        if (pos.y -1 >= 0 )
            positionsToCheck.Add(pos + Vector2Int.down);
        if (pos.x + 1 < editedTexture.width)
            positionsToCheck.Add(pos + Vector2Int.right);
        if (pos.x -1 >= 0)
            positionsToCheck.Add(pos + Vector2Int.left);

        foreach (Vector2Int pixel in positionsToCheck)
        {
            if(!pixelsToCheck.Contains(pixel) && IsSamePixelColor(color, GetPixelColorAtPosition(pixel)))
            {
                pixelsToCheck.Add(pixel);
            }
        }         
    }

    void AddPixelToColorArray(Vector2Int pix, Color color){               
        int pixelPosition = (pix.x + (pix.y * editedTexture.height));
        colorArray[pixelPosition] = color;        
    }

    Color GetPixelColorAtPosition(Vector2Int pos)
    {
        return editedTexture.GetPixel(pos.x, pos.y);
    }

    bool IsSamePixelColor(Color color1, Color color2)
    {
        return color1 == color2; 
    }

    #endregion

    #region UIButtons
    public void ChangeColor(string colorName)
    {
        switch(colorName){
            case "red":
                brushColor = Color.red;
                break;
            case "black":
                brushColor = Color.black;
                break;
            case "white":
                brushColor = Color.white;
                break;
            case "blue":
                brushColor = Color.blue;
                break;
            case "green":
                brushColor = Color.green;
                break;
            case "pink":
                brushColor = Color.magenta;
                break;
        }

        actualColorUI.color = brushColor;
    }
        
    public void SelectAnotherBodyPart(string bodyPartName)
    {
        switch (bodyPartName)
        {
            case "Head":
                SetATextureToDraw(bodySprites[0]);
                break;
            case "Belly":
                SetATextureToDraw(bodySprites[1]);
                break;
            case "ArmL":
                SetATextureToDraw(bodySprites[2]);
                break;
            case "ArmR":
                SetATextureToDraw(bodySprites[3]);
                break;
            case "LegL":
                SetATextureToDraw(bodySprites[4]);
                break;
            case "LegR":
                SetATextureToDraw(bodySprites[5]);
                break;
        }
    }

    public void ChangeTool(string selectedTool)
    {
        activeTool = selectedTool;
        selectedToolText.text = activeTool;
    }

    public void ResetImage()
    {
        ResetEveryTexture();
    }

    public void ChangeBrushSize()
    {
        brushSize = Mathf.RoundToInt(brushSizeBar.value * 100);
        brushSizeText.text = brushSize.ToString();
    }

    #endregion
          
    #region Set And Reset Textures
    private void SetATextureToDraw(Sprite selectedSprite)
    {
        editedTexture = selectedSprite.texture;

        //Pour l'affichage de la texture en taille reelle, pour visualiser
        //sera supprimable quand tout fonctionnera bien
        editedTextureRectTransform.sizeDelta = new Vector2(256, 256);
    }

    private void ResetEveryTexture()
    {
        //Remeber the last used texture to set it back after the reset
        Texture2D lastUsedTexture = editedTexture;

        foreach(Sprite spr in bodySprites)
        {
            editedTexture = spr.texture;

            ResetTexture();
        }

        editedTexture = lastUsedTexture;
    }

    public void ResetTexture()
    {
        Color lastcolor = brushColor;

        brushColor = eraseColor;

        editedTexture.Resize(256, 256);

        editedTextureRectTransform.sizeDelta = new Vector2(256, 256);

        CheckIfPositionIsPaintBucketable(Vector2Int.zero);

        brushColor = lastcolor;
    }
    #endregion

    #region Texture Projection

    private void OnGUI()
    {
        //Enlever l'actualisation de la position quand on sera sur que l'ecran ne bougera pas
        ActualizeDrawingZonePosition();

        GUI.DrawTextureWithTexCoords(textureProjectionRect, editedTexture, textureOffsetRect, false);
    }

    void ActualizeDrawingZonePosition()
    {
        //This allows the Drawing Zone to follow the Size and Positition of a Rect on the scene.
        //So it is easier to position it on screen.

        //Set Width and Height and Position from the Rect on scene
        drawRect = drawZoneRectTrans.rect;
        drawRect.x = drawZoneRectTrans.anchoredPosition.x;
        drawRect.y = drawZoneRectTrans.anchoredPosition.y;

        textureProjectionRect = drawRect;
        textureProjectionRect.y = (Screen.height - textureProjectionRect.height) - drawRect.y;
    }
    #endregion

}
